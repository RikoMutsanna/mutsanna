package com.example.mutsanna;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MutsannaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MutsannaApplication.class, args);
	}

}
